import numpy as np
import pandas as pd
import cv2
import os
import csv
from tqdm import tqdm


############################## generating bounding-box match file ################################


def generate_csv_file(path, filename):

    if not os.path.exists(path + filename + '.csv'):

        with open(path + filename + '.csv', 'w') as csvfile:

            csv_file = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

            csv_file.writerow(['Well_field', 'Columbus_cells', 'Acapella_cells', 'bbox'])


def append_df_to_csv_file(df, csv_file):

    dfr = pd.DataFrame(np.array(df))

    with open(csv_file, 'a') as csv_file:

        dfr.to_csv(csv_file, mode='a', header=False)

    csv_file.close()


def row_to_dict(row):

    well_field = str(row['well_field'])
    cell = int(row['cell'])
    x1 = int(row['leftX'])
    x2 = int(row['rightX'])
    y1 = int(row['topY'])
    y2 = int(row['bottomY'])

    bb_dict = {'well_field': well_field, 'cell': cell, 'x1': x1, 'x2': x2, 'y1': y1, 'y2': y2}

    return bb_dict


def get_iou(cbb, abb):

    assert cbb['x1'] < cbb['x2']
    assert cbb['y1'] < cbb['y2']
    assert abb['x1'] < abb['x2']
    assert abb['y1'] < abb['y2']

    x_left = max(cbb['x1'], abb['x1'])
    y_top = max(cbb['y1'], abb['y1'])
    x_right = min(cbb['x2'], abb['x2'])
    y_bottom = min(cbb['y2'], abb['y2'])

    if x_right < x_left or y_bottom < y_top:

        iou = 0.0

    else:

        intersection_area = (x_right - x_left) * (y_bottom - y_top)

        cbb_area = (cbb['x2'] - cbb['x1']) * (cbb['y2'] - cbb['y1'])
        abb_area = (abb['x2'] - abb['x1']) * (abb['y2'] - abb['y1'])

        iou = intersection_area / float(cbb_area + abb_area - intersection_area)

        assert iou >= 0.0
        assert iou <= 1.0

    c_well_field, ccell, acell = str(cbb['well_field']), str(cbb['cell']), str(abb['cell'])

    iou = format(iou, '.3f')

    tag = [c_well_field, ccell, acell, iou]

    return tag


def generate_columbus_df(path, columbus_csv_file):

    """
    converting Columbus csv file to pandas dataframe

    Args:
        path: full path to csv file.
        columbus_csv_file: csv file name.

    Returns:
        Dataframe where each row represents a valid cell including bounding box coordinates
    """

    field_cols = ['n', 'WellName', 'Field', 'BoundingBox', 'goodCell']

    columbus_df = pd.read_csv(path + columbus_csv_file, usecols=field_cols)

    columbus_df['well_field'] = columbus_df['WellName'].map(str) + '-' + columbus_df['Field'].map(str)

    columbus_df['BoundingBox'] = columbus_df['BoundingBox'].str.replace('[', '')

    columbus_df['BoundingBox'] = columbus_df['BoundingBox'].str.replace(']', '')

    columbus_bb = columbus_df['BoundingBox'].str.split(pat=',', expand=True)

    columbus_bb.columns = ['leftX', 'topY', 'rightX', 'bottomY']

    columbus_bb['well'], columbus_bb['well_field'], columbus_bb['cell'], columbus_bb['goodCell'] = columbus_df['WellName'], columbus_df['well_field'], columbus_df['n'], columbus_df['goodCell']

    columbus_bb = columbus_bb[['well', 'well_field', 'cell', 'goodCell', 'leftX', 'topY', 'rightX', 'bottomY']]

    columbus_bb = columbus_bb.loc[columbus_bb['goodCell'] == 1]

    return columbus_bb


def generate_acapella_df(path, acapella_bbox_file):

    """
    converting Acapella bounding box file to pandas dataframe

    Args:
        path: full path to Acapella txt file.
        columbus_csv_file: bbox file name.

    Returns:
        Dataframe where each row represents a cropped cell including bounding box coordinates
    """

    field_cols = ['filename', 'leftX', 'topY', 'rightX', 'bottomy']

    acapella_df = pd.read_csv(path + acapella_bbox_file, delimiter="\t", usecols=field_cols)

    acapella_df['well'] = acapella_df['filename'].map(lambda x: chr(64 + int((x.split('_')[1])[1:3])) + str(int((x.split('_')[1])[4:6])))

    acapella_df['field'] = acapella_df['filename'].map(lambda x: str((x.split('_')[1])[8:9]))

    acapella_df['cell'] = acapella_df['filename'].map(lambda x: str((x.split('_')[2]).split('-')[0]))

    acapella_df['well_field'] = acapella_df['well'].map(str) + '-' + acapella_df['field'].map(str)

    acapella_df['bottomY'] = acapella_df['bottomy']

    acapella_bb = acapella_df[['filename', 'well', 'well_field', 'cell', 'leftX', 'topY', 'rightX', 'bottomY']]

    return acapella_bb


def compare_subset_by_well(columbus_subset, acapella_subset):

    IOUs = []

    columbus_dict = []

    for index, row in tqdm(columbus_subset.iterrows()):

        dict_row = row_to_dict(row)

        columbus_dict.append(dict_row)

    acapella_dict = []

    for index, row in tqdm(acapella_subset.iterrows()):

        dict_row = row_to_dict(row)

        acapella_dict.append(dict_row)

    for cbb in tqdm(columbus_dict):

        for abb in acapella_dict:

            IOU = get_iou(cbb, abb)

            if float(IOU[3]) > 0.7:

                IOUs.append(IOU)

    return IOUs


def generate_bbox_matchfile(folder_path, columbus_csv_file, acapella_bbox_file, csv_filename):

    """
    Generating bbox matchfile

    Args:
        folder_path: full path to image folder.
        columbus_csv_file:
        acapella_bbox_file:
        csv_filename:

    Returns:
        writes results to a csv file
    """

    csvfile = folder_path + csv_filename + '.csv'

    generate_csv_file(path=folder_path, filename=csv_filename)

    columbus_df = generate_columbus_df(path=folder_path, columbus_csv_file=columbus_csv_file)

    acapella_df = generate_acapella_df(path=folder_path, acapella_bbox_file=acapella_bbox_file)

    columbus_wells = columbus_df['well_field'].unique().tolist()

    for well in tqdm(columbus_wells):

        columbus_subset = columbus_df.loc[columbus_df['well_field'] == well]

        acapella_subset = acapella_df.loc[acapella_df['well_field'] == well]

        IOUs = compare_subset_by_well(columbus_subset, acapella_subset)

        append_df_to_csv_file(IOUs, csvfile)


######################################### loading images ##########################################


def load_images(path):

    """
        Loading images from folder

        Args:
            path: full path to image folder.

        Returns:
            two lists: the first is of arrays of images and the second is a list of labels
            label format : well-field-cell number (e.g. B12-3-453)
            note that cell number corresponds to cell numbers in Acapella bounding box file
        """

    images = []

    labels = []

    count = 0

    for image in tqdm(os.listdir(path)):

        if image.lower().endswith('tiff'):

            handle = path + '/' + image

            image_file = np.array(cv2.imread(handle))

            well = chr(64 + int((image.split('_')[1])[1:3])) + str(int((image.split('_')[1])[4:6]))

            field = (image.split('_')[1])[8:9]

            cell_number = (image.split('_')[2]).split('-')[0]

            label = well + '-' + str(field) + '-' + str(cell_number)

            images.append(image_file)

            labels.append(label)

            count = count + 1

    dataset = images, labels

    return dataset

    pass


#################################### generate multicell images #######################################


def generate_canvas(dims):

    canvas = np.ndarray(dims)

    return canvas


def valid_cells(dataframe):

    valid_list = dataframe.loc()

    return valid_list


def append_cell(canvas, cell):


    '''
    we have three options:
    1. extract contours from acapella
    2. crop from bounding box to contour with python
    3. append cells to grid and design special convolution layer to scan individual cells
    '''



