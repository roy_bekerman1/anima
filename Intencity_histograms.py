import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def load_data(path, csv_file):

    pixels_df = pd.read_csv(path + csv_file)

    pixels_df.columns = ['pixels', 'well']

    # pixels_df['row'] = pixels_df['well'].map(str)[:1]

    # pixels_df['row'] = pixels_df['well'].map(lambda x: str(x[:1]))

    # pixels_df['colomn'] = pixels_df['well'].map(str)[1:]

    # pixels_df['colomn'] = pixels_df['well'].map(lambda x: str(x[1:]))

    return pixels_df


def generate_pivot(dataframe):

    table = pd.pivot_table(dataframe, values=['pixels'], index=['well'], aggfunc={'pixels': [np.mean, np.std]})

    return table


def subset_by_well(dataframe, well):

    subset = dataframe.loc[dataframe['well'] == well]

    return subset


def well_list(dataframe):

    wells = dataframe['well'].unique().tolist()

    return wells


def dataframe_histogram(dataframe):

    fig = dataframe.hist(column='pixels', bins=1000)

    return fig


if __name__ == "__main__":

    path = '/Users/roybekerman/Desktop/'

    csv_file = '2_1_5th_spots_sum.csv'

    main_dataframe = load_data(path=path, csv_file=csv_file)

    subset1 = subset_by_well(main_dataframe, 'B10')

    subset2 = subset_by_well(main_dataframe, 'B23')

    fig = plt.figure()

    ax = fig.add_subplot(111)

    subset1.hist(ax=ax, alpha=0.6, color='red', bins=100)

    subset2.hist(ax=ax, alpha=0.3, color='green', bins=100)

    plt.show()


