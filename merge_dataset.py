import pandas as pd
from timeit import default_timer as timer
import time
import csv
import os
import numpy as np


def create_columbus_df(columbus_csv_file):


    field_cols = ['n', 'WellName', 'Field', 'BoundingBox', 'goodCell']

    columbus_df = pd.read_csv(folder_path + columbus_csv_file, usecols=field_cols)

    columbus_df['well_field'] = columbus_df['WellName'].map(str) + '-' + columbus_df['Field'].map(str)

    columbus_df['BoundingBox'] = columbus_df['BoundingBox'].str.replace('[', '')

    columbus_df['BoundingBox'] = columbus_df['BoundingBox'].str.replace(']', '')

    columbus_bb = columbus_df['BoundingBox'].str.split(pat=',', expand=True)

    columbus_bb.columns = ['leftX', 'topY', 'rightX', 'bottomY']

    columbus_bb['well'], columbus_bb['well_field'], columbus_bb['cell'], columbus_bb['goodCell'] = columbus_df['WellName'], columbus_df['well_field'], columbus_df['n'], columbus_df['goodCell']

    columbus_bb = columbus_bb[['well', 'well_field', 'cell', 'goodCell', 'leftX', 'topY', 'rightX', 'bottomY']]

    columbus_bb = columbus_bb.loc[columbus_bb['goodCell'] == 1]

    return columbus_bb


def create_acapella_df(acapella_bbox_file):


    field_cols = ['filename', 'leftX', 'topY', 'rightX', 'bottomy']

    acapella_df = pd.read_csv(folder_path + acapella_bbox_file, delimiter="\t", usecols=field_cols)

    acapella_df['well'] = acapella_df['filename'].map(lambda x: chr(64 + int((x.split('_')[1])[1:3])) + str(int((x.split('_')[1])[4:6])))

    acapella_df['field'] = acapella_df['filename'].map(lambda x: str((x.split('_')[1])[8:9]))

    acapella_df['cell'] = acapella_df['filename'].map(lambda x: str((x.split('_')[2]).split('-')[0]))

    acapella_df['well_field'] = acapella_df['well'].map(str) + '-' + acapella_df['field'].map(str)

    acapella_df['bottomY'] = acapella_df['bottomy']

    acapella_bb = acapella_df[['filename', 'well', 'well_field', 'cell', 'leftX', 'topY', 'rightX', 'bottomY']]

    return acapella_bb


def well_list(dataframe, field):

    wells = dataframe[field].unique().tolist()

    return wells


def generate_well_subset(df, well):

    subset = df.loc[df['well_field'] == well]

    return subset


def row_to_dict(row):

    well_field = str(row['well_field'])
    cell = int(row['cell'])
    x1 = int(row['leftX'])
    x2 = int(row['rightX'])
    y1 = int(row['topY'])
    y2 = int(row['bottomY'])

    bb_dict = {'well_field': well_field, 'cell': cell, 'x1': x1, 'x2': x2, 'y1': y1, 'y2': y2}

    return bb_dict


def get_iou(cbb, abb):

    assert cbb['x1'] < cbb['x2']
    assert cbb['y1'] < cbb['y2']
    assert abb['x1'] < abb['x2']
    assert abb['y1'] < abb['y2']

    x_left = max(cbb['x1'], abb['x1'])
    y_top = max(cbb['y1'], abb['y1'])
    x_right = min(cbb['x2'], abb['x2'])
    y_bottom = min(cbb['y2'], abb['y2'])

    if x_right < x_left or y_bottom < y_top:

        iou = 0.0

    else:

        intersection_area = (x_right - x_left) * (y_bottom - y_top)

        cbb_area = (cbb['x2'] - cbb['x1']) * (cbb['y2'] - cbb['y1'])
        abb_area = (abb['x2'] - abb['x1']) * (abb['y2'] - abb['y1'])

        iou = intersection_area / float(cbb_area + abb_area - intersection_area)

        assert iou >= 0.0
        assert iou <= 1.0

    c_well_field, ccell, acell = str(cbb['well_field']), str(cbb['cell']), str(abb['cell'])

    iou = format(iou, '.3f')

    tag = [c_well_field, ccell, acell, iou]

    return tag


def compare_subset_by_well(columbus_subset, acapella_subset):

    IOUs = []

    columbus_dict = []

    for index, row in columbus_subset.iterrows():

        dict_row = row_to_dict(row)

        columbus_dict.append(dict_row)

    acapella_dict = []

    for index, row in acapella_subset.iterrows():

        dict_row = row_to_dict(row)

        acapella_dict.append(dict_row)

    for cbb in columbus_dict:

        for abb in acapella_dict:

            IOU = get_iou(cbb, abb)

            if float(IOU[3]) > 0.7:

                IOUs.append(IOU)

    return IOUs


def generate_csv_file(path, filename):

    if not os.path.exists(path + filename + '.csv'):

        with open(path + filename + '.csv', 'w') as csvfile:

            csv_file = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

            csv_file.writerow(['Well_field', 'Columbus_cells', 'Acapella_cells', 'bbox'])


def append_df_to_csv_file(df, csv_file):

    dfr = pd.DataFrame(np.array(df))

    with open(csv_file, 'a') as csv_file:

        dfr.to_csv(csv_file, mode='a', header=False)

    csv_file.close()


def generate_bbox_match_file(folder_path, columbus_csv_file, acapella_bbox_file, csv_filename):

    csvfile = folder_path + csv_filename + '.csv'

    start = timer()
    t = time.localtime()
    start_time = time.strftime("%H:%M:%S", t)

    print('Process started at %s' % start_time)

    generate_csv_file(path=folder_path, filename=csv_filename)

    columbus_df = create_columbus_df(columbus_csv_file=columbus_csv_file)
    acapella_df = create_acapella_df(acapella_bbox_file=acapella_bbox_file)

    print('%s images loaded from Columbus' % len(columbus_df))
    print('%s images loaded from Acapella' % len(acapella_df))

    columbus_wells = columbus_df['well_field'].unique().tolist()

    print('%s wells found' % len(columbus_wells))

    count = 0

    t = time.localtime()
    match_time = time.strftime("%H:%M:%S", t)

    print('Matching bounding boxes started at %s' % match_time)

    for well in columbus_wells:

        columbus_subset = generate_well_subset(columbus_df, well)
        acapella_subset = generate_well_subset(acapella_df, well)

        IOUs = compare_subset_by_well(columbus_subset, acapella_subset)

        append_df_to_csv_file(IOUs, csvfile)

        count += 1

        if count % 10 == 0:

            ratio = format(float(count*100) / float(len(columbus_wells)), '.4f')

            print('Well %s matching complete, %s cells matched, %s percent of total' % (well, len(IOUs), ratio))

        else:

            print('Well %s matching complete, %s cells matched' % (well, len(IOUs)))

    end = timer()
    total_time = format(((end - start)/60), '.1f')

    print('Total processe time was %s minutes' % total_time)

    t = time.localtime()
    end_time = time.strftime("%H:%M:%S", t)

    print('Finished processing at %s' % end_time)


def generate_pivot_table(df, key, field):

    pivot_table = pd.DataFrame(df.groupby(key)[field].count())

    # pivot_table = pivot_table[['well_field', 'cells']]

    # pivot_table = pivot_table.set_index('well_field')

    return pivot_table


def generate_report(columbus_pivot, acapella_pivot, bbox_pivot):

    colac_table = columbus_pivot.join(acapella_pivot)

    report_table = colac_table.join(bbox_pivot)

    # report_table = report_table[['well_field', 'columbus', 'acapella', 'bbox']]

    # report_table['well'] = report_table['well_field'].map(lambda x: str(x.split('_')[0]))

    # grouped_by_well = report_table.groupby('well').sum()

    return report_table


if __name__ == "__main__":

    folder_path = '/Users/roybekerman/Desktop/ANL/2-2/'
    columbus_csv_file = 'allCell_WithSelected11_ANL_Pilot2_P2_2_2703.csv'
    acapella_bbox_file = 'bboxANL2-2-31-3.txt'
    csv_filename = '2-2_clean_IOU_matchfile.csv'

    generate_bbox_match_file(folder_path=folder_path, columbus_csv_file=columbus_csv_file, acapella_bbox_file=acapella_bbox_file, csv_filename=csv_filename)
    #
    # columbus_pivot = generate_pivot_table(columbus_df, 'well_field', 'cell')
    # acapella_pivot = generate_pivot_table(acapella_df, 'well_field', 'filename')
    # bbox_pivot = generate_pivot_table(bbox_df, 'Well', 'IOU')
    #
    # grouped_by_well = generate_report(columbus_pivot, acapella_pivot, bbox_pivot)

    print('Finished generating match file')

