import numpy as np
import cv2
import matplotlib.pyplot as plt

path = '/Users/roybekerman/Desktop/'

# cropped cFRET
# origin_folder = 'ANL/2-1/Cropped_images/cFRET/'
# image = 'fifth_r02c02f01p01-ch5sk1fk1fl1.tiff_6-1032.tiff'

# cropped DAPI-Cy5
# origin_folder = 'ANL/2-1/Cropped_images/DAPI-Cy5/'
# image = 'unite_r02c02f01p01-ch5sk1fk1fl1.tiff_6-1032.tiff'

# cropped cFRET-DAPI
# origin_folder = 'ANL/2-1/Cropped_images/cFRET-DAPI/'
# image = 'fifth_r02c02f01p01-ch5sk1fk1fl1.tiff_6-1032.tiff'

# S3 DAPI
# origin_folder = 'ANL/2-1/S3_Images/'
# image = 'r02c02f01p01-ch1sk1fk1fl1.tiff'
# image = 'r11c06f01p01-ch1sk1fk1fl1.tiff'

# S3 Cy3
# origin_folder = 'ANL/2-1/S3_Images/'
# image = 'r02c02f01p01-ch2sk1fk1fl1.tiff'
# image = 'r11c06f01p01-ch2sk1fk1fl1.tiff'

# S3 Cy5
# origin_folder = 'ANL/2-1/S3_Images/'
# image = 'r02c02f01p01-ch3sk1fk1fl1.tiff'
# image = 'r11c06f01p01-ch3sk1fk1fl1.tiff'

# S3 FRET
# origin_folder = 'ANL/2-1/S3_Images/'
# image = 'r02c02f01p01-ch4sk1fk1fl1.tiff'
# image = 'r11c06f01p01-ch4sk1fk1fl1.tiff'

# S3 cFRET
# origin_folder = 'ANL/2-1/S3_cFRET/Images/'
# image = 'r02c04f03p01-ch5sk1fk1fl1.tiff'

# Microscopy
# origin_folder = 'microscopy/'
# image = 'microscopy5.jpg'

# Hubble ultra-deep field
# origin_folder = 'hudf/'
# image = 'hudf4.jpg'

# Cats
# origin_folder = 'cats/'
# image = 'cat3.jpg'


handle = path + origin_folder + image
image_file = np.array(cv2.imread(handle, 0))
fig = plt.figure(figsize=(8, 4))
ax = fig.add_subplot(121)
ax.imshow(image_file[..., ::-1])
ax = fig.add_subplot(122)
x = image_file.ravel()
plt.hist(x[x>50], 256, [0, 256])
plt.xlabel('Pixel value')
plt.ylabel('Pixel count')
plt.title('Pixel Intensity Histogram')
plt.show()
