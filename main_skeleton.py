""" Anima Biotech main skeleton for image preprocessing """


import numpy as np
import pandas as pd
import csv
import os
import cv2
import time
from keras import callbacks
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D, ZeroPadding2D, BatchNormalization
from keras.utils import to_categorical
from sklearn.preprocessing import LabelEncoder
from keras.optimizers import SGD
from timeit import default_timer as timer


""" Load images and datafiles """


def generate_columbus_df(path, columbus_csv_file):

    """
    converting Columbus csv file to pandas dataframe

    Args:
        path: full path to csv file.
        columbus_csv_file: csv file name.

    Returns:
        Dataframe where each row represents a valid cell including bounding box coordinates
    """

    field_cols = ['n', 'WellName', 'Field', 'BoundingBox', 'goodCell']

    columbus_df = pd.read_csv(path + columbus_csv_file, usecols=field_cols)

    columbus_df['well_field'] = columbus_df['WellName'].map(str) + '-' + columbus_df['Field'].map(str)

    columbus_df['BoundingBox'] = columbus_df['BoundingBox'].str.replace('[', '')

    columbus_df['BoundingBox'] = columbus_df['BoundingBox'].str.replace(']', '')

    columbus_bb = columbus_df['BoundingBox'].str.split(pat=',', expand=True)

    columbus_bb.columns = ['leftX', 'topY', 'rightX', 'bottomY']

    columbus_bb['well'], columbus_bb['well_field'], columbus_bb['cell'], columbus_bb['goodCell'] = columbus_df['WellName'], columbus_df['well_field'], columbus_df['n'], columbus_df['goodCell']

    columbus_bb = columbus_bb[['well', 'well_field', 'cell', 'goodCell', 'leftX', 'topY', 'rightX', 'bottomY']]

    columbus_bb = columbus_bb.loc[columbus_bb['goodCell'] == 1]

    return columbus_bb


def generate_acapella_df(path, acapella_bbox_file):

    """
    converting Acapella bounding box file to pandas dataframe

    Args:
        path: full path to Acapella txt file.
        columbus_csv_file: bbox file name.

    Returns:
        Dataframe where each row represents a cropped cell including bounding box coordinates
    """

    field_cols = ['filename', 'leftX', 'topY', 'rightX', 'bottomy']

    acapella_df = pd.read_csv(path + acapella_bbox_file, delimiter="\t", usecols=field_cols)

    acapella_df['well'] = acapella_df['filename'].map(lambda x: chr(64 + int((x.split('_')[1])[1:3])) + str(int((x.split('_')[1])[4:6])))

    acapella_df['field'] = acapella_df['filename'].map(lambda x: str((x.split('_')[1])[8:9]))

    acapella_df['cell'] = acapella_df['filename'].map(lambda x: str((x.split('_')[2]).split('-')[0]))

    acapella_df['well_field'] = acapella_df['well'].map(str) + '-' + acapella_df['field'].map(str)

    acapella_df['bottomY'] = acapella_df['bottomy']

    acapella_bb = acapella_df[['filename', 'well', 'well_field', 'cell', 'leftX', 'topY', 'rightX', 'bottomY']]

    return acapella_bb


def load_images(path, image_type):

    """
    Loading images from folder

    Args:
        path: full path to image folder.
        image_type: type of image (e.g png, jpeg)

    Returns:
        two lists: the first is of arrays of images and the second is a list of labels
        label format : well-field-cell number (e.g. B12-3-453)
        note that cell number corresponds to cell numbers in Acapella bounding box file
    """

    images = []

    labels = []

    for image in os.listdir(path):

        if image.lower().endswith(image_type):

            handle = path + '/' + image

            image_file = np.array(cv2.imread(handle))

            # well = chr(64 + int((image.split('_')[1])[1:3])) + str(int((image.split('_')[1])[4:6]))
            #
            # field = (image.split('_')[1])[8:9]
            #
            # cell_number = (image.split('_')[2]).split('-')[0]
            #
            # label = well + '-' + str(field) + '-' + str(cell_number)

            label = image.split('.')[0]

            images.append(image_file)

            labels.append(label)

    dataset = images, labels

    return dataset


"""Generate hand-crafted feature vectors"""


def generate_hcf(path, hcf_list):

    """
    generating pandas dataframe with hand-crafted features

    Args:
        path: full path to Columbus csv file.
        hcf_list: list of hand crafted features in csv format.

    Returns:
        Dataframe of hand-crafted features from valid cells only.
    """

    field_cols = hcf_list

    columbus_df = pd.read_csv(path, usecols=field_cols)

    columbus_df['well_field'] = columbus_df['WellName'].map(str) + '-' + columbus_df['Field'].map(str)

    hcf_vectors = columbus_df.loc[columbus_df['goodCell'] == 1]

    return hcf_vectors


"""Generate Columbus/Acapella match-file by aligning bounding boxes"""


def generate_well_list(dataframe, field):

    wells = dataframe[field].unique().tolist()

    return wells


def generate_well_subset(df, well):

    subset = df.loc[df['well_field'] == well]

    return subset


def row_to_dict(row):

    well_field = str(row['well_field'])
    cell = int(row['cell'])
    x1 = int(row['leftX'])
    x2 = int(row['rightX'])
    y1 = int(row['topY'])
    y2 = int(row['bottomY'])

    bb_dict = {'well_field': well_field, 'cell': cell, 'x1': x1, 'x2': x2, 'y1': y1, 'y2': y2}

    return bb_dict


def get_iou(cbb, abb):

    assert cbb['x1'] < cbb['x2']
    assert cbb['y1'] < cbb['y2']
    assert abb['x1'] < abb['x2']
    assert abb['y1'] < abb['y2']

    x_left = max(cbb['x1'], abb['x1'])
    y_top = max(cbb['y1'], abb['y1'])
    x_right = min(cbb['x2'], abb['x2'])
    y_bottom = min(cbb['y2'], abb['y2'])

    if x_right < x_left or y_bottom < y_top:

        iou = 0.0

    else:

        intersection_area = (x_right - x_left) * (y_bottom - y_top)

        cbb_area = (cbb['x2'] - cbb['x1']) * (cbb['y2'] - cbb['y1'])
        abb_area = (abb['x2'] - abb['x1']) * (abb['y2'] - abb['y1'])

        iou = intersection_area / float(cbb_area + abb_area - intersection_area)

        assert iou >= 0.0
        assert iou <= 1.0

    c_well_field, ccell, acell = str(cbb['well_field']), str(cbb['cell']), str(abb['cell'])

    iou = format(iou, '.3f')

    tag = [c_well_field, ccell, acell, iou]

    return tag


def compare_subset_by_well(columbus_subset, acapella_subset):

    IOUs = []

    columbus_dict = []

    for index, row in columbus_subset.iterrows():

        dict_row = row_to_dict(row)

        columbus_dict.append(dict_row)

    acapella_dict = []

    for index, row in acapella_subset.iterrows():

        dict_row = row_to_dict(row)

        acapella_dict.append(dict_row)

    for cbb in columbus_dict:

        for abb in acapella_dict:

            IOU = get_iou(cbb, abb)

            if float(IOU[3]) > 0.7:

                IOUs.append(IOU)

    return IOUs


def generate_csv_file(path, filename):

    if not os.path.exists(path + filename + '.csv'):

        with open(path + filename + '.csv', 'w') as csvfile:

            csv_file = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

            csv_file.writerow(['Well_field', 'Columbus_cells', 'Acapella_cells', 'bbox'])


def append_df_to_csv_file(df, csv_file):

    dfr = pd.DataFrame(np.array(df))

    with open(csv_file, 'a') as csv_file:

        dfr.to_csv(csv_file, mode='a', header=False)

    csv_file.close()


def generate_bbox_matchfile(folder_path, columbus_csv_file, acapella_bbox_file, csv_filename):

    """
    Generating bbox matchfile

    Args:
        folder_path: full path to image folder.
        columbus_csv_file:
        acapella_bbox_file:
        csv_filename:

    Returns:
        writes results to a csv file
    """

    csvfile = folder_path + csv_filename + '.csv'

    generate_csv_file(path=folder_path, filename=csv_filename)

    columbus_df = generate_columbus_df(path=folder_path, columbus_csv_file=columbus_csv_file)

    acapella_df = generate_acapella_df(path=folder_path, acapella_bbox_file=acapella_bbox_file)

    columbus_wells = columbus_df['well_field'].unique().tolist()

    count = 0

    for well in columbus_wells:

        columbus_subset = generate_well_subset(columbus_df, well)

        acapella_subset = generate_well_subset(acapella_df, well)

        IOUs = compare_subset_by_well(columbus_subset, acapella_subset)

        append_df_to_csv_file(IOUs, csvfile)

        count += 1

        if count % 10 == 0:

            ratio = format(float(count*100) / float(len(columbus_wells)), '.4f')

            print('Well %s matching complete, %s cells matched, %s percent of total' % (well, len(IOUs), ratio))

        else:

            print('Well %s matching complete, %s cells matched' % (well, len(IOUs)))


"""filtering invalid cells"""


def generate_valid_cell_list():
    pass


def cell_innumeration():
    pass


"""image padding and normalization"""


def image_padding(images, labels, new_size):

    padded_images = []

    padded_labels = []

    for image_file, label in zip(images, labels):

        height = image_file.shape[0]

        width = image_file.shape[1]

        d_height = new_size - height

        d_width = new_size - width

        if d_height > 0 and d_width > 0:

            t = d_height // 2

            b = d_height - (d_height // 2)

            l = d_width // 2

            r = d_width - (d_width // 2)

            c = [0, 0, 0]

            padded_image = cv2.copyMakeBorder(np.array(image_file), t, b, l, r, cv2.BORDER_CONSTANT, value=c)

            norm_image = cv2.normalize(padded_image, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

            padded_images.append(norm_image)

            padded_labels.append(label)

    padded_dataset = padded_images, padded_labels

    return padded_dataset


def image_normalization(image_file):

    norm_image = cv2.normalize(image_file, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

    return norm_image


"""generating train and test sets"""


def split_dataset(images, labels, test_size):

    X = []

    y = []

    for image_file, label in zip(images, labels):

        well = label.split('-')[0]

        colomn = well[:1]

        if colomn == 2 or colomn == 23:

            label = 'Control'

        else:

            label = well

        X.append(image_file)

        y.append(label)

    encoder = LabelEncoder()

    encoder.fit(y)

    y = np.array(encoder.transform(y).astype(np.int32))

    X = np.array(X)

    number_of_classes = len(np.unique(y))

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=1)

    y_train = to_categorical(y_train)

    y_test = to_categorical(y_test)

    split_dataset = X_train, X_test, y_train, y_test, number_of_classes

    return split_dataset


"""defining neural model"""


def vgg10(image_size, number_of_classes):

    model = Sequential()

    model.add(ZeroPadding2D((1, 1), input_shape=image_size))
    model.add(Conv2D(32, (3, 3), strides=(1, 1), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(32, (3, 3), strides=(1, 1), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(32, (3, 3), strides=(1, 1), activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(64, (3, 3), strides=(1, 1), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(64, (3, 3), strides=(1, 1), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(64, (3, 3), strides=(1, 1), activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(Flatten())

    model.add(Dense(2048, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2048, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1024, activation='relu'))

    model.add(Dense(number_of_classes, activation='softmax'))

    return model


"""training and saving model"""


def train(dataset, model, model_path, batch_size, epochs):

    X_train, X_test, y_train, y_test, number_of_classes = zip(dataset)

    tensorboard = callbacks.TensorBoard(log_dir=logdir, histogram_freq=2, write_graph=True, write_grads=True, write_images=False, update_freq=update_frequency)

    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True, clipvalue=1)

    model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['accuracy'])

    model.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(X_test, y_test), shuffle=True, callbacks=[tensorboard])

    model.evaluate(X_test, y_test, verbose=1)

    model.save(model_path)


if __name__ == "__main__":

    main_path = '/Users/roybekerman/Desktop/ANL/2-1/'

    dataset_path = '/Users/roybekerman/Desktop/Full_DB/Plate2_1-5thImages/'

    columbus_csv_file = 'allCell_WithSelected11_ANL_Pilot2_P2_12_2703.csv'

    acapella_bbox_file = 'bboxANL2-1-31-3.txt'

    iou_filename = 'new2-1_clean_IOU_matchfile.csv'

    model_name = 'model_name'

    image_type = '.tiff'

    save_folder = '/Anima_models'

    save_dir = main_path + save_folder

    logdir = main_path + save_folder + model_name

    model_path = os.path.join(save_dir, model_name)

    image_size = 80

    test_size = 0.2

    batch_size = 32

    epochs = 100

    update_frequency = 'batch'

    dataset = load_images(path=dataset_path, image_type=image_type)

    images = dataset[0]

    labels = dataset[1]

    padded_dataset = image_padding(images=images, labels=labels, new_size=image_size)

    padded_images = padded_dataset[0]

    padded_labels = padded_dataset[1]

    split_dataset = split_dataset(images=padded_images, labels=padded_labels, test_size=test_size)

    model = vgg10(image_size=image_size, number_of_classes=split_dataset[4])

    train(dataset=split_dataset, model=model, model_path=model_path, batch_size=batch_size, epochs=epochs)




