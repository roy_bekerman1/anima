import numpy as np
import os
import cv2
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D, ZeroPadding2D
from keras.utils import to_categorical
from datetime import datetime
from sklearn.preprocessing import LabelEncoder
from keras.optimizers import SGD, rmsprop
import seaborn as sns
from matplotlib import pyplot as plt


now = datetime.today().isoformat()
path = '/Users/roybekerman/Desktop'
number_of_images = 1000


def load_imageset(path):

    source_file = '/ANL/3/Images/DAPI_Cy5'
    image_type = '.tiff'
    images = []
    labels = []
    count = 0

    for image in os.listdir(path + source_file):

        if image.lower().endswith(image_type) and count < number_of_images:

            handle = path + source_file + '/' + image
            image_file = np.array(cv2.imread(handle, 0))
            label = chr(64 + int((image.split('_')[1])[1:3])) + str(int((image.split('_')[1])[4:6]))
            channel = int((image.split('-')[1])[2:3])

            if channel == 1:
                channel_name = 'DAPI'
            elif channel == 2:
                channel_name = 'Cy3'
            elif channel == 3:
                channel_name = 'Cy5'
            elif channel == 4:
                channel_name = 'FRET'
            else:
                channel_name = 'cFRET'

            images.append(image_file)
            labels.append(label)
            count = count + 1

    dataset = [images, labels]

    print("%s images loaded" % number_of_images)

    return dataset


def imageset_analysis():

    widths = []
    heights = []
    sizes = []

    dataset = load_imageset(path)
    images = dataset[0]

    for image in images:

        image_width = int(image.shape[0])
        image_height = int(image.shape[1])
        image_size = image_width * image_height
        widths.append(image_width)
        heights.append(image_height)
        sizes.append(image_size)

    image_count = len(widths)
    avg_width = np.mean(widths)
    avg_height = np.mean(heights)
    avg_size = np.mean(sizes)

    imageset_data = [widths, heights, sizes, image_count, avg_width, avg_height, avg_size]

    print("imageset avg. width is %s pixels" % avg_width)
    print("imageset avg. height is %s pixels" % avg_height)

#    sns.jointplot(widths, heights, kind="kde",height=7, space=0, xlim=200, ylim=200).set_axis_labels("Width", "Hieght")
#    sns.scatterplot(x=widths, y=heights, marker="+", size=10, color=".2")

    f, axes = plt.subplots(2, 2, figsize=(7, 7), sharex=False, sharey=False)
    sns.despine(left=True)
    sns.distplot(widths, hist=True, kde=True, rug=True, color=".2", ax=axes[0, 0])
    sns.jointplot(widths, heights, kind="kde",color=".2", height=7, space=0, xlim=200, ylim=200, ax=axes[0, 1]).set_axis_labels("Width", "Hieght")
    sns.scatterplot(x=widths, y=heights, marker="+", size=10, color=".2", ax=axes[1, 0])

    plt.setp(axes, yticks=[])
    plt.tight_layout()

    plt.show()

    return imageset_data


def padding():

    X = []
    y = []

    images = load_imageset(path)[0]
    labels = load_imageset(path)[1]

    new_size = 224

    for image, label in zip(images, labels):

        height = image.shape[0]
        width = image.shape[1]

        d_height = new_size - height
        d_width = new_size - width

        if d_height > 0 and d_width > 0:

            t = d_height // 2
            b = d_height - (d_height // 2)
            l = d_width // 2
            r = d_width - (d_width // 2)
            c = [0, 0, 0]

            padded_image = cv2.copyMakeBorder(np.array(image), t, b, l, r, cv2.BORDER_CONSTANT, value=c)
            X.append(padded_image)
            y.append(label)

    padded_imageset = [X, y]

    # print("image padding to %s by %s pixels is complete" % (new_size, new_size))

    return padded_imageset


def split_imageset():

    test_size = 0.2

    images = padding()[0]
    labels = padding()[1]
    encoder = LabelEncoder()
    encoder.fit(labels)
    y = np.array(encoder.transform(labels).astype(np.int32))
    X = np.array(images)

    number_of_classes = len(np.unique(y))
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=1)
    split_dataset = [X_train, X_test, y_train, y_test, number_of_classes]

    print("image dataset was split to %d images for training and %d images for testing" % (len(X_train), len(X_test)))

    return split_dataset


def train_cnn():

    save_folder = '/Anima models'
    model_name = 'Anima-' + now + '.h5'
    save_dir = path + save_folder

    batch_size = 8
    epochs = 1

    X_train = split_imageset()[0]
    X_test = split_imageset()[1]
    y_train = split_imageset()[2]
    y_test = split_imageset()[3]
    number_of_classes = split_imageset()[4]

    y_train = to_categorical(y_train)
    y_test = to_categorical(y_test)

    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same', input_shape=X_train.shape[1:]))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(number_of_classes))
    model.add(Activation('softmax'))

    opt = rmsprop(lr=0.0001, decay=1e-6)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    model.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(X_test, y_test), shuffle=True)

    model_path = os.path.join(save_dir, model_name)
    model.save(model_path)
    print('Saved trained CNN at %s ' % model_path)

    scores = model.evaluate(X_test, y_test, verbose=1)
    print('Test loss:', scores[0])
    print('Test accuracy:', scores[1])


def vgg16():

    save_folder = '/Anima models'
    model_name = 'Anima-' + now + '.h5'
    save_dir = path + save_folder

    batch_size = 32
    epochs = 1

    X_train = split_imageset()[0]
    X_test = split_imageset()[1]
    y_train = split_imageset()[2]
    y_test = split_imageset()[3]
    number_of_classes = split_imageset()[4]

    y_train = to_categorical(y_train)
    y_test = to_categorical(y_test)

    model = Sequential()
    model.add(ZeroPadding2D((1, 1), input_shape=X_train.shape[1:]))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2, 2)))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(128, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(128, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2, 2)))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(256, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(256, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(256, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(512, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(512, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(512, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(512, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(512, (3, 3), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(512, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(Flatten())
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(number_of_classes, activation='softmax'))

    sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['accuracy'])
    model.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(X_test, y_test), shuffle=True)

    model_path = os.path.join(save_dir, model_name)
    model.save(model_path)
    print('Saved trained VGG16 model at %s ' % model_path)

    scores = model.evaluate(X_test, y_test, verbose=1)
    print('Test loss:', scores[0])
    print('Test accuracy:', scores[1])


#load_imageset(path)
#images = imageset[0]
#print(len(images))
#imageset_analysis()
# padding = padding()
# split_imageset()
# train_cnn()
# vgg16()




