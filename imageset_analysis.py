import numpy as np
import os
import cv2
from datetime import datetime
import seaborn as sns
from matplotlib import pyplot as plt
import collections
import time

now = time.time()
path = '/Users/roybekerman/Desktop'

source_file = '/ANL/3/Cropped images/cFRET-DAPI'

image_type = '.tiff'
count = 0
number_of_images = 1000000

images = []
labels = []
widths = []
heights = []

start = now
for image in os.listdir(path + source_file):

    if image.lower().endswith(image_type) and count < number_of_images:

        handle = path + source_file + '/' + image
        image_file = np.array(cv2.imread(handle, 0))
        # label = chr(64 + int((image.split('-')[0])[1:3])) + str(int((image.split('-')[0])[4:6]))
        image_width = int(image_file.shape[0])
        image_height = int(image_file.shape[1])

        images.append(image_file)
        # labels.append(label)
        widths.append(image_width)
        heights.append(image_height)

        count = count + 1

end = now

image_count = len(widths)
avg_width = np.mean(widths)
avg_height = np.mean(heights)
# label_counter = collections.Counter(labels)
# label_keys = label_counter.keys()
# label_values = label_counter.values()

print("%s images loaded" % image_count)
print("imageset avg. width is %s pixels" % avg_width)
print("imageset avg. height is %s pixels" % avg_height)
# print(label_counter)

f, axes = plt.subplots(2, 2, figsize=(7, 7), sharex=False, sharey=False)
sns.despine(left=True)
sns.distplot(widths, hist=True, kde=True, rug=True, color=".2", ax=axes[0, 0])
sns.jointplot(widths, heights, kind="kde",color=".2", height=7, space=0, xlim=200, ylim=200, ax=axes[0, 1]).set_axis_labels("Width", "Hieght")
sns.scatterplot(x=widths, y=heights, marker="+", size=10, legend=False, color=".2", ax=axes[1, 0])
#sns.distplot(labels, hist=True, kde=True, rug=True, color=".2", ax=axes[1, 1])
#sns.barplot(x="total", y="abbrev", data=labels, label="Total", color="b", ax=axes[1, 1])

plt.setp(axes, yticks=[])
plt.tight_layout()

plt.show()



# crashes = sns.load_dataset("car_crashes").sort_values("total", ascending=False)



# Add a legend and informative axis label
# ax.legend(ncol=2, loc="lower right", frameon=True)
# ax.set(xlim=(0, 24), ylabel="", xlabel="Automobile collisions per billion miles")
