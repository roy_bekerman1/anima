import numpy as np
import os
import cv2

version = '1M'
path = '/Users/roybekerman/Desktop/'
disk_path = '/Volumes/Transcend/Anima/plates/'

plate = '2_2'

# channel = '5thImages'
channel = 'dapiImages'
# channel = '5thdapiImages'

# origin_folder = 'ANL/2-1/Cropped_images/cFRET'
# origin_folder2 = 'ANL/2-1/Cropped_images/DAPI-Cy5'
# origin_folder = 'ANL_Pilot2_P2_1__2019-01-10T02_17_49-Measurement1/output/'
origin_folder = 'ANL_Pilot2_P2_2__2019-01-10T04_01_58-Measurement1/output/'
# origin_folder = 'Overfit-300_plate2_1-'

# filename = 'pixel_sum'

destination_folder = 'nDB' + '-' + str(version) + '-' + 'plate' + str(plate) + '-' + str(channel)
image_type = '.tiff'

# number_of_classes = 50
# number_of_cells = 10
# totalcells = 100000

# if os.path.exists((path + destination_folder)):
#     os.remove(path + destination_folder)

os.makedirs(path + destination_folder)

# image_count = 0
# well_pixels = []

for image in os.listdir(disk_path + origin_folder + channel):

    # if image.lower().endswith(image_type) and image_count < totalcells:
    if image.lower().endswith(image_type):

        handle = disk_path + origin_folder + channel + '/' + image
        image_file = np.array(cv2.imread(handle, 0))
        well = chr(64 + int((image.split('_')[1])[1:3])) + str(int((image.split('_')[1])[4:6]))
        field = str((image.split('_')[1])[8:9])
        cell = str((image.split('_')[2]).split('-')[0])
        # pixel_sum = int(image_file.sum())
        # well_pixel = well + '-' + cell + '-' + str(pixel_sum)
        # well_pixels.append(well_pixel)
        label = well + '-' + field + '-' + cell + image_type
        cv2.imwrite(os.path.join(path + destination_folder, label), image_file)


