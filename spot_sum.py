import numpy as np
import os
import cv2
import csv
import pandas as pd


path = '/Users/roybekerman/Desktop/'
source_file = 'Full_DB/Plate2_2-dapiImages'
csv_file_name = '2_2_Dapi_spots_sum'
csv_file = path + csv_file_name + '.csv'
image_type = '.tiff'

labels = []
pixels = []

count = 0

with open(csv_file, 'a', newline='') as csvfile:

    fieldnames = ['Label','Spot_sum']

    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    for image in os.listdir(path + source_file):

        if image.lower().endswith(image_type):

            handle = path + source_file + '/' + image

            image_file = np.array(cv2.imread(handle, 0))

            label = image.split('-')[0]

            pixel_sum = int(image_file.sum())

            labels.append(label)

            pixels.append(pixel_sum)

            count += 1

            if count%50000 == 0:

                print('%s images loaded' % count)

                df = pd.DataFrame(labels, pixels)

                df.to_csv(csv_file, mode='a', header=False)

                labels = []

                pixels = []

