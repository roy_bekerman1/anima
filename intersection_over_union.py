import pandas as pd
import os
from timeit import default_timer as timer
import numpy as np
import time


path = '/Users/roybekerman/Desktop/ANL/2-1/'
nrows = 100000


def create_columbus_df():

    columbus_csv_file = 'allCell_WithSelected11_ANL_Pilot2_P2_12_2703.csv'
    field_cols = ['n', 'WellName', 'Field', 'BoundingBox', 'goodCell']

    columbus_df = pd.read_csv(path + columbus_csv_file, usecols=field_cols, skiprows=skiprows, nrows=nrows)
    columbus_df['well_field'] = columbus_df['WellName'].map(str) + '-' + columbus_df['Field'].map(str)
    columbus_df['BoundingBox'] = columbus_df['BoundingBox'].str.replace('[', '')
    columbus_df['BoundingBox'] = columbus_df['BoundingBox'].str.replace(']', '')
    columbus_bb = columbus_df['BoundingBox'].str.split(pat=',', expand=True)
    columbus_bb.columns = ['left X', 'top Y', 'right X', 'bottom Y']
    columbus_bb['well_field'], columbus_bb['cell'] = columbus_df['well_field'], columbus_df['n']
    columbus_bb = columbus_bb[['well_field', 'cell', 'left X', 'top Y', 'right X', 'bottom Y']]
    grouped_columbus_bb = columbus_bb.groupby(['well_field'])

    columbus_dict = []

    for group_name, group_df in grouped_columbus_bb:
        for index, row in group_df.iterrows():

            cwf = group_name
            ccell = int(row['cell'])
            cx1 = int(row['left X'])
            cx2 = int(row['right X'])
            cy1 = int(row['top Y'])
            cy2 = int(row['bottom Y'])

            cbb = {'well_field': cwf, 'cell': ccell, 'x1': cx1, 'x2': cx2, 'y1': cy1, 'y2': cy2}

            columbus_dict.append(cbb)

    return columbus_dict


def create_acapella_df():

    acapella_bbox_data = 'bboxANL2-1-31-3.txt'
    acapella_df = pd.read_csv(path + acapella_bbox_data, delimiter="\t", nrows=nrows, skiprows=skiprows)

    wellnames = []
    fields = []
    cells = []

    for file in acapella_df['filename']:
        wellname = chr(64 + int((file.split('_')[1])[1:3])) + str(int((file.split('_')[1])[4:6]))
        field = str((file.split('_')[1])[8:9])
        cell = str((file.split('_')[2]).split('-')[0])
        wellnames.append(wellname)
        fields.append(field)
        cells.append(cell)

    acapella_df['wellname'], acapella_df['field'], acapella_df['cell'] = wellnames, fields, cells
    acapella_df['well_field'] = acapella_df['wellname'].map(str) + '-' + acapella_df['field'].map(str)
    acapella_bb = acapella_df[['well_field', 'cell', 'leftX', 'topY', 'rightX', 'bottomy']]

    grouped_acapella_bb = acapella_bb.groupby(['well_field'])

    acapella_dict = []

    for group_name, group_df in grouped_acapella_bb:
        for index, row in group_df.iterrows():

            awf = group_name
            acell = int(row['cell'])
            ax1 = int(row['leftX'])
            ax2 = int(row['rightX'])
            ay1 = int(row['topY'])
            ay2 = int(row['bottomy'])

            abb = {'well_field': awf, 'cell': acell, 'x1': ax1, 'x2': ax2, 'y1': ay1, 'y2': ay2}

            acapella_dict.append(abb)

    return acapella_dict


def get_iou(cbb, abb):

    assert cbb['x1'] < cbb['x2']
    assert cbb['y1'] < cbb['y2']
    assert abb['x1'] < abb['x2']
    assert abb['y1'] < abb['y2']

    x_left = max(cbb['x1'], abb['x1'])
    y_top = max(cbb['y1'], abb['y1'])
    x_right = min(cbb['x2'], abb['x2'])
    y_bottom = min(cbb['y2'], abb['y2'])

    if x_right < x_left or y_bottom < y_top:
        iou = 0.0

    else:

        intersection_area = (x_right - x_left) * (y_bottom - y_top)

        cbb_area = (cbb['x2'] - cbb['x1']) * (cbb['y2'] - cbb['y1'])
        abb_area = (abb['x2'] - abb['x1']) * (abb['y2'] - abb['y1'])

        iou = intersection_area / float(cbb_area + abb_area - intersection_area)
        assert iou >= 0.0
        assert iou <= 1.0

    ccell = str(cbb['cell'])
    acell = str(abb['cell'])

    c_well_field = str(cbb['well_field'])
    a_well_field = str(abb['well_field'])

    tag = (c_well_field, ccell, a_well_field, acell, iou)

    return tag


t = time.localtime()
start_time = time.strftime("%H:%M:%S", t)
print('Process started at %s' % start_time)

start = timer()

count = 0

while count < 11:

    skiprows = range(1, int(count*100000))

    columbus_df = create_columbus_df()
    acapella_df = create_acapella_df()

    IOUs = []

    for cbb in columbus_df:
        for abb in acapella_df:
            if cbb['well_field'] == abb['well_field']:
                IOU = get_iou(cbb, abb)
                if IOU[4] > 0.5:
                    IOUs.append(IOU)

    IOUs = np.asarray(IOUs)
    np.savetxt(path + 'ANL 2-1 IOU 10K-' + str(count) + '.csv', IOUs, delimiter=',', fmt='%s')

    end = timer()
    print('Batch %d processed in %d minutes' % (count, ((end - start)/60)))

    t = time.localtime()
    end_time = time.strftime("%H:%M:%S", t)
    print('Finished processing batch #%d at %s' % (count, end_time))

    count = count + 1


