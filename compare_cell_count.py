import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

path = '/Users/roybekerman/Desktop/'
csv_file = 'ANL/2-1/allCell_WithSelected11_ANL_Pilot2_P2_12_2703.csv'
bbox_data = 'ANL/2-1/bboxANL2-1-31-3.txt'

nrows = 10000000
usecols_columbus = ['WellName', 'Field', 'x']
usecols = ['WellName', 'Field', 'x', 'y', 'BoundingBox', 'goodCell']

columbus_df = pd.read_csv(path + csv_file, usecols=usecols_columbus, nrows=nrows)
columbus_df.rename(columns={'x': 'Columbus'}, inplace=True)
columbus_df["well_field"] = columbus_df["WellName"].map(str) + '-' + columbus_df["Field"].map(str)
columbus_grouped = columbus_df.groupby(['well_field'])
columbus = columbus_grouped['Columbus'].count()

acapella_df = pd.read_csv(path + bbox_data, delimiter="\t", nrows=nrows)
acapella_df.rename(columns={'leftX': 'Acapella'}, inplace=True)
wellnames = []
fields = []
for file in acapella_df["filename"]:
    wellname = chr(64 + int((file.split('_')[1])[1:3])) + str(int((file.split('_')[1])[4:6]))
    field = str((file.split('_')[1])[8:9])
    wellnames.append(wellname)
    fields.append(field)
acapella_df["wellname"], acapella_df["field"] = wellnames, fields
acapella_df["well_field"] = acapella_df["wellname"].map(str) + '-' + acapella_df["field"].map(str)
acapella_grouped = acapella_df.groupby(['well_field'])
acapella = acapella_grouped['Acapella'].count()

cell_count = pd.merge(columbus, acapella, on='well_field', how='outer')
cell_count["dif"] = cell_count["Columbus"] - cell_count["Acapella"]

# sns.scatterplot(data=cell_count, x='Columbus', y='Acapella', marker="+", size=10, legend=False, color=".2")

# plt.show()
# print("%d images in Acapella, %d images in Columbus" % (len(acapella_df), len(columbus_df)))

# print(np.mean(cell_count["Acapella"]))
