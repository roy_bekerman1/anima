import cv2
import os

path = '/Users/roybekerman/Desktop/Full_DB/'

fret_origin_folder = 'Plate2_2-5thImages/'

dapi_origin_folder = 'Plate2_2-dapiImages/'

destination_folder = 'BlendDB-2_2'

image_type = '.tiff'


alpha = 0.03

beta = (1 - float(alpha))

count = 0


os.makedirs(path + destination_folder)

fret_images = sorted(os.listdir(path + fret_origin_folder))

dapi_images = sorted(os.listdir(path + dapi_origin_folder))

for fret_image, dapi_image in zip(fret_images, dapi_images):

        if str(fret_image) == str(dapi_image):

            fret_handle = path + fret_origin_folder + '/' + fret_image

            fret_image_file = cv2.imread(fret_handle, 0)

            dapi_handle = path + dapi_origin_folder + '/' + dapi_image

            dapi_image_file = cv2.imread(dapi_handle, 0)

            try:

                blend_image = cv2.addWeighted(dapi_image_file, alpha, fret_image_file, beta, 0)

                label = str(fret_image)

                cv2.imwrite(os.path.join(path + destination_folder, label), blend_image)

                count += 1

            except:

                pass

            if count%1000 == 0:

                print('%s Images created successfuly' % count)

