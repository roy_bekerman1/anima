import numpy as np
import os
import cv2
import time
from keras import callbacks
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D, ZeroPadding2D
from keras.utils import to_categorical
from sklearn.preprocessing import LabelEncoder
from keras.optimizers import SGD
from timeit import default_timer as timer


net_version = 'vgg10-ofer'

now = timer()

path = 'main_path'

source_file = 'path_to_image_folder'

image_type = '.tiff'

save_folder = '/Anima_models'

model_name = 'Anima_overfit' + str(net_version) + '.h5'

save_dir = path + save_folder

logdir = path + save_folder + '/Anima_overfit/' + str(net_version)

model_path = os.path.join(save_dir, model_name)


images = []

labels = []

X = []

y = []


new_size = 80

test_size = 0.2

batch_size = 16

epochs = 100

update_frequency = 'batch'

start = timer()

for image in os.listdir(path + source_file):

    if image.lower().endswith(image_type):

        handle = path + source_file + '/' + image

        image_file = np.array(cv2.imread(handle))

        well = chr(64 + int((image.split('_')[1])[1:3])) + str(int((image.split('_')[1])[4:6]))

        field = (image.split('_')[1])[8:9]

        cell_number = (image.split('_')[2]).split('-')[0]

        label = well + '-' + str(field)

        colomn = well[:1]

        if colomn == 2 or colomn == 23:

            label = 'Control'

        else:

            label = label

        images.append(image_file)

        labels.append(label)

print("%s images loaded" % len(images))

srart_padding = time.time()

for image, label in zip(images, labels):

    height = image.shape[0]

    width = image.shape[1]

    d_height = new_size - height

    d_width = new_size - width

    if d_height > 0 and d_width > 0:

        t = d_height // 2

        b = d_height - (d_height // 2)

        l = d_width // 2

        r = d_width - (d_width // 2)

        c = [0, 0, 0]

        padded_image = cv2.copyMakeBorder(np.array(image), t, b, l, r, cv2.BORDER_CONSTANT, value=c)

        norm_image = cv2.normalize(padded_image, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

        X.append(norm_image)

        y.append(label)

print("image padding of %s images to %s by %s pixels is complete" % (len(y), new_size, new_size))

encoder = LabelEncoder()

encoder.fit(y)

y = np.array(encoder.transform(y).astype(np.int32))

X = np.array(X)

number_of_classes = len(np.unique(y))

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=1)

print("%s classes found" % number_of_classes)

print("image dataset was split to %d images for training and %d images for testing" % (len(X_train), len(X_test)))

end_preprocessing = timer()

preprocessing = end_preprocessing - start

print("Total preprocessing time %s seconds" % preprocessing)

y_train = to_categorical(y_train)

y_test = to_categorical(y_test)


def vgg10():

    model = Sequential()

    model.add(ZeroPadding2D((1, 1), input_shape=(X_train.shape[1:])))
    model.add(Conv2D(32, (3, 3), strides=(1, 1), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(32, (3, 3), strides=(1, 1), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(32, (3, 3), strides=(1, 1), activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(64, (3, 3), strides=(1, 1), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(64, (3, 3), strides=(1, 1), activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Conv2D(64, (3, 3), strides=(1, 1), activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))

    model.add(Flatten())

    model.add(Dense(2048, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2048, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1024, activation='relu'))

    model.add(Dense(number_of_classes, activation='softmax'))

    return model


if __name__ == "__main__":

    model = vgg10()

    tensorboard = callbacks.TensorBoard(log_dir=logdir, histogram_freq=2, write_graph=True, write_grads=True, write_images=False, update_freq=update_frequency)

    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True, clipvalue=1)

    model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['accuracy'])

    model.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, validation_data=(X_test, y_test), shuffle=True, callbacks=[tensorboard])

    model.save(model_path)

    scores = model.evaluate(X_test, y_test, verbose=1)

    end = timer()

    total_time = end - start

    print("Total run time %s seconds" % total_time)